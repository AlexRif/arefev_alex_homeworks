public abstract class Figure {
    private double x = 0;
    private double y = 0;

    public abstract double getPerimetr();

    public void setCoordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return y;
    }

}
