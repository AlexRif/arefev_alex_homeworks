import java.util.Random;

public class Main {
    public static void main(String[] args) {
        //создание массива объеков классов, в которых реалтзован интерфейс перемещения фигур
        Square[] square = new Square[10];
        Circle[] circles = new Circle[10];
        for(int i=0;i< circles.length;i++) { // Цикл записи данных в массив
            circles[i] = new Circle();
            circles[i].setCoordinates( 1, 1);
        }
        for(int i=0;i< circles.length;i++) { // Цикл записи данных в массив
            square[i] = new Square();
            square[i].setCoordinates( 1, 1);
        }
        //вывод данных о координатах
        for(int i=0;i< circles.length;i++) {
            System.out.printf("Координаты круга %d: x = %.2f y = %.2f\n", i+1, circles[i].getX(), circles[i].getY());
        }
        for(int i=0;i< square.length;i++) {
            System.out.printf("Координаты квадрата %d: x = %.2f y = %.2f\n", i+1, square[i].getX(), square[i].getY());
        }
    }
}
