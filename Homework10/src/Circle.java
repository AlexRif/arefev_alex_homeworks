public class Circle extends Ellipse implements Movement{
    public Circle(double radius) {
        super(radius, radius);
    }
    public Circle() {
    }
    public void movement(double x, double y) {
        setCoordinates(x, y);
    }
}
