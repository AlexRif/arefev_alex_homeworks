public class Ellipse extends Figure {
    private double radius1;
    private double radius2;

    public Ellipse(double radius1, double radius2) {
        if (radius1 > 0 && radius2 > 0) {
            this.radius1 = radius1;
            this.radius2 = radius2;
        }
    }
    public Ellipse() {
    }
    public double getRadius1() {
        return radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    public double getPerimetr() {
        return 3.14 * (radius1 + radius2);
    }
}

