public class Square extends Rectangle implements Movement {
    public Square(double a) {
        super(a, a);
    }
    public Square() {
    }
    public void movement( double x, double y ) {
        setCoordinates(x, y);
    }
}
