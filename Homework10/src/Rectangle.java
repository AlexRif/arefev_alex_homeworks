public class Rectangle extends Figure {
    private double a;
    private double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }
    public Rectangle() {
    }
    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public double getPerimetr() {
        return 2*(a+b);
    }
}

